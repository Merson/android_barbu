package com.example.luc_o.barbu_de_luco;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.luc_o.barbu_de_luco.Storage.Card;
import com.example.luc_o.barbu_de_luco.Storage.Deck;

import org.w3c.dom.Text;

public class WelcomeActivity extends AppCompatActivity {

    //Vars
    private TextView _card_name_text_view;
    private TextView _card_rule_text_view;
    private TextView _card_count_text_view;

    private Deck _deck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        this.setDeck();
    }

    private void setDeck() {
        this._deck = new Deck();
        this._deck.shuffle_the_deck();

        this._card_name_text_view = (TextView) findViewById(R.id.text_view_card_name);
        this._card_rule_text_view = (TextView) findViewById(R.id.text_view_card_rule);
        this._card_count_text_view = (TextView) findViewById(R.id.text_view_card_count);

        this._card_name_text_view.setText("Le jeu du barbu par Luc-Olivier Merson");
        this._card_rule_text_view.setText("Tirez une carte pour commencer la partie !");
        this._card_count_text_view.setText("Il y a " + this._deck._cards.size() + " dans le deck");
    }

    public void on_pick_a_card_clicked(View v) {
        Card tmp = this._deck.draw_a_card();
        if (tmp != null) {
            this._card_name_text_view.setText(tmp._name + " de " + tmp.getType());
            this._card_rule_text_view.setText(tmp._rule);
            this._card_count_text_view.setText("Il reste " + this._deck._cards.size()  + " dans le deck");
        } else {
            this.setDeck();
        }
    }
}

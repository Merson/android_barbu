package com.example.luc_o.barbu_de_luco.Storage;

/**
 * Created by luc-o on 15/09/2017.
 */

public class Card {

    //Consts
    public final static int TYPE_CLUB = 0;
    public final static int TYPE_DIAMOND = 1;
    public final static int TYPE_HEART = 2;
    public final static int TYPE_SPADE = 3;

    //Vars
    public int _id;
    public int _type;
    public String _name;
    public String _rule;
    public int _ressource_id;

    public Card(int id, int type, String name, String rule, int ressource_id) {
        this._id = id;
        this._type = type;
        this._name = name;
        this._rule = rule;
        this._ressource_id = ressource_id;
    }

    public String getType() {
        String type = "";
        if (this._type == Card.TYPE_CLUB) {
            type =  "trèfle";
        } else if (this._type == Card.TYPE_DIAMOND) {
            type =  "carreau";
        } else if (this._type == Card.TYPE_HEART) {
            type =  "coeur";
        } else if (this._type == Card.TYPE_SPADE) {
            type =  "pique";
        }
        return type;
    }

    @Override
    public String toString() {
        String tmp = "";
        String type = "";
        if (this._type == Card.TYPE_CLUB) {
            type =  "trèfle";
        } else if (this._type == Card.TYPE_DIAMOND) {
            type =  "carreau";
        } else if (this._type == Card.TYPE_HEART) {
            type =  "heart";
        } else if (this._type == Card.TYPE_SPADE) {
            type =  "pique";
        }
        tmp += "" + this._id + " " + this._name + " de" + type + " " + this._rule + "\n";
        return tmp;
    }
}

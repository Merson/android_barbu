package com.example.luc_o.barbu_de_luco.Storage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

/**
 * Created by luc-o on 15/09/2017.
 */

public class Deck {
    public int _max_size;
    public ArrayList<Card> _cards;

    public Deck(int max_size) {
        System.out.println("Un deck vide a été généré");
        this._max_size = max_size;
        this._cards = new ArrayList<>();
    }

    public Deck() {
        System.out.println("Un deck basique a été généré");
        this._cards = new ArrayList<>();
        for (int i = 0; i < 4; ++i) {
            this.add_a_card(new Card(1, i, "As", "Prends une gorgée", 0));
            this.add_a_card(new Card(2, i, "Deux", "Prends deux gorgée", 0));
            this.add_a_card(new Card(3, i, "Trois", "Prends trois gorgée", 0));
            this.add_a_card(new Card(4, i, "Quatre", "Distribue quatre gorgée", 0));
            this.add_a_card(new Card(5, i, "Cinq", "Distribue cinq gorgée", 0));
            this.add_a_card(new Card(6, i, "Six", "Distribue six gorgée", 0));
            this.add_a_card(new Card(7, i, "Sept", "Le jeu du dans ma valise il y a ...", 0));
            this.add_a_card(new Card(8, i, "Huit", "Le jeu du je n'ai jamais", 0));
            this.add_a_card(new Card(9, i, "Neuf", "Le jeu des lettres", 0));
            this.add_a_card(new Card(10, i, "Dix", "Le jeu du thème", 0));
            this.add_a_card(new Card(11, i, "Valet", "Roi des pouces", 0));
            this.add_a_card(new Card(12, i, "Dame", "La cascade !", 0));
            this.add_a_card(new Card(13, i, "Roi", "Invente une règle", 0));
        }
        this._max_size = this._cards.size();
    }

    public void shuffle_the_deck() {
        System.out.println("Le jeu a été mélangé");
        Collections.shuffle(this._cards);
    }

    public void add_a_card(Card c) {
        this._cards.add(c);
    }

    public Card draw_a_card() {
        if (this._cards.size() > 0) {
            Random r = new Random();
            int number = r.nextInt(this._cards.size());
            Card tmp = this._cards.get(number);
            this._cards.remove(number);
            System.out.println("J'ai tiré la carte numéro " + number + ", il reste " + this._cards.size() + " cartes dans le deck");
            return (tmp);
        }
        System.out.println("Il n'y a plus de cartes dans le deck");
        return null;
    }

    @Override
    public String toString() {
        String tmp = "";
        tmp += "Max size of the deck = " + this._max_size + "\n";
        tmp += "Size of the deck = " + this._cards.size() + "\n";
        for (int i = 0; i < this._cards.size(); ++i) {
            tmp += this._cards.get(i).toString();
        }
        return tmp;
    }
}
